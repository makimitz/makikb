+++
title = "Index"
description = ""
weight = 100
hidden = false
+++

# Cheatsheet <3

Acknowledgement :

| Nickname | Twitter | Resources |
|----------|---------|-----------|
| Swissky  | [@pentest_swissky](https://twitter.com/pentest_swissky) | https://github.com/swisskyrepo/PayloadsAllTheThings |
| \_ACKNAK_  | [@\_ACKNAK_](https://twitter.com/_ACKNAK_) | https://acknak.fr/en/ | 
| Haax     | [@Haaxmax](https://twitter.com/Haaxmax) | https://twitter.com/Haaxmax |
| Aperi'kube | [@AperiKube](https://twitter.com/AperiKube) | https://www.aperikube.fr/ |